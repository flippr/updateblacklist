<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 10/22/15
 * Time: 9:45 PM
 */

namespace BlacklistUpdate\Services;

use PDO;

class Connection extends PDO
{

    protected $dbname = '';
    protected $host = '';
    protected $user = '';
    protected $pass = '';

    public function __construct()
    {
        try {
            parent::__construct("mysql:host=" . $this->host . ";dbname=" . $this->dbname, $this->user, $this->pass);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->setAttribute(PDO::ATTR_PERSISTENT, true);
        }
        catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
}