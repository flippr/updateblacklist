<?php

/**
 * Created by PhpStorm.
 * User: josh
 * Date: 10/22/15
 * Time: 4:58 PM
 */

namespace BlacklistUpdate\Services;

use Mailgun\Mailgun;

class EmailValidate
{
    public static function checkAddresses($mailgun_public_key, $validateAddress)
    {
        # Instantiate the client.
        $mgClient = new Mailgun($mailgun_public_key);

        # Issue the call to the client.
        $result = $mgClient->get("address/validate", array('address' => $validateAddress));

        return $result->http_response_body->is_valid;
    }
}