<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 10/22/15
 * Time: 8:12 PM
 */

namespace BlacklistUpdate\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use BlacklistUpdate\Services\Connection;
use Mailgun\Mailgun;
use PDO;

class EmailUpdateCommand extends Command
{
    protected $db;

    protected function configure()
    {
        $this->setName('update:email')->setDescription('Assigns proper role to users based on validity of their email address');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->db = new Connection();
        $this->grabSupressions();
    }

    private function grabSupressions()
    {
        $mailgun_key = 'key-d9833f41e0937f6add481c111e5270d4';
        $mgClient = new Mailgun($mailgun_key);
        $domain = 'mmea.net';
        $batch = 100;
        $types = array(
            'bounces',
            'unsubscribes',
            'complaints'
        );
        foreach ($types as $type) {
            # Issue the call to the client.
            $count_result = $mgClient->get("$domain/$type");

            $count = $count_result->http_response_body->total_count;
            //$units = ceil($count / $batch);
            $result = json_decode($this->createReturnBounces($mailgun_key, $type), false);

            for ($i = 1; $i <= $count; $i += $batch) {
                foreach ($result->items as $item) {
                    $this->checkBlacklist($item->address);
                }
                $result = json_decode($this->createReturnBounces($mailgun_key, $type, $result->paging->next), false);
            }
        }
    }

    private function createReturnBounces($mailgun_key, $type, $url = NULL)
    {
        $username = 'api';
        $ch = curl_init();
        if ($url) {
            curl_setopt($ch, CURLOPT_URL, $url);
        }
        else {
            curl_setopt($ch, CURLOPT_URL, "https://api.mailgun.net/v3/mmea.net/$type");
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: Basic " . base64_encode($username . ":" . $mailgun_key)
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);

        return $server_output;
    }


    private function checkBlacklist($recipient)
    {

        try {
            $conn = $this->db;

            $sql = "SELECT id, email FROM mmea_user_email_bounce WHERE email = '{$recipient}' AND id > 0 LIMIT 1";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_OBJ);
            if (isset($row) && $row != NULL) {
                //intentionally left blank
            }
            else {
                $this->insertSingleBlacklistedRecipient($recipient);

                return true;
            }
        }
        catch (\Exception $e) {
            throw $e;
        }

        return false;
    }

    private function insertSingleBlacklistedRecipient($recipient)
    {
        $conn = $this->db;
        if (isset($recipient) && $recipient != NULL) {
            try {
                $sql = "INSERT IGNORE INTO mmea_user_email_bounce SET id = NULL, email = '{$recipient}'";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

                return true;
            }
            catch (\Exception $e) {
                throw $e;
            }
        }
        else {
            return false;
        }
    }
}