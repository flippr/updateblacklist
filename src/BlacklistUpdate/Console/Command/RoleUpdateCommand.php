<?php
/**
 * Created by PhpStorm.
 * User: josh
 * Date: 10/22/15
 * Time: 7:44 PM
 */

namespace BlacklistUpdate\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BlacklistUpdate\Services\EmailValidate;
use BlacklistUpdate\Services\Connection;
use PDO;

class RoleUpdateCommand extends Command
{

    protected $db;
    protected $valid_role = 14;
    protected $invalid_role = 15;
    protected $pubkey = 'pubkey-5ogiflzbnjrljiky49qxsiozqef5jxp7';
    protected $config;

    protected function configure()
    {
        $this->setName('update:roles')->setDescription('Assigns proper role to users based on validity of their email address');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        try {
            $this->db = new Connection();
        }
        catch (\Exception $e) {
            throw $e;
        }
        $this->findUsersWithoutRoles();
        $this->findUsersWithRoles();
    }

    private function findUsersWithoutRoles()
    {
        try {
            $stmt = $this->db->prepare("SELECT u.uid, u.mail
                            FROM mmea_users u
                            LEFT JOIN mmea_users_roles ur
                            ON u.uid = ur.uid
                            WHERE ur.rid is null");
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (isset($users)) {
                foreach ($users as $user) {
                    $this->checkUserAgainstBlacklist($user);
                }
            }
        }
        catch (\Exception $e) {
            throw $e;
        }

        return false;
    }

    private function findUsersWithRoles()
    {
        try {
            $conn = $this->db;

            $sql = "
                SELECT u.uid, u.mail
                FROM mmea_users u
                LEFT JOIN mmea_users_roles ur
                ON u.uid = ur.uid
                WHERE ur.rid is not null
                ";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $users = $stmt->fetchAll(PDO::FETCH_OBJ);

            if (isset($users)) {
                foreach ($users as $user) {
                    $this->checkUserAgainstBlacklist($user);
                }
            }
        }
        catch (\Exception $e) {
            throw $e;
        }

        return false;
    }

    private function checkUserAgainstBlacklist($user)
    {
        $invalid = array();
        $valid = array();

        try {
            $conn = $this->db;
            $email = $user->mail;
            $sql = "SELECT id, email FROM mmea_user_email_bounce WHERE email = '{$email}' AND id > 0 LIMIT 1";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_OBJ);

            /*
             * If user DOES exist in the blacklisted table, add the role of invalid
             */
            if (isset($row) && $row != NULL) {
                $invalid[] = $user->uid;
                //$this->addUserToRole($user, $this->invalid_role);
            }
            /*
             * If user DOESN'T exist in blacklisted table, check mailgun stuff.
             */
            else {
                if (EmailValidate::checkAddresses($this->pubkey, $user)) {
                    $valid[] = $user->uid;
                    $this->addUserToRole($user, $this->valid_role);
                }
                else {
                    $invalid[] = $user->uid;
                    //$this->addUserToRole($user, $this->invalid_role);
                }
            }

            $this->addUserToRole($valid, $this->valid_role);
            $this->addUserToRole($invalid, $this->invalid_role);
        }
        catch (\Exception $e) {
            throw $e;
        }

        return false;
    }

    private function addUserToRole(array $users, $role)
    {
        //print_r($user);
        $conn = $this->db;
        if (isset($users)) {
            $stmt = $conn->prepare("INSERT IGNORE INTO mmea_users_roles(uid,rid) VALUES (:uid,:rid)");

            $conn->beginTransaction();
            try {
                foreach ($users as $user) {
                    $stmt->execute(array(
                        ':uid' => $user,
                        ':rid' => $role,

                    ));
                }
                $conn->commit();
            }
            catch (\Exception $e) {
                $conn->rollback();
                throw $e;
            }
            /*try {
                $sql = "INSERT INTO mmea_users_roles SET uid=:uid,rid=:rid";
                if ($stmt = $conn->prepare($sql)) {
                    $stmt->execute(array(
                        ':uid' => $user->uid,
                        ':rid' => $role
                    ));

                    if ($stmt->rowCount() > 0) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    // intentionally left blank
                }
            }
            catch (\Exception $e) {
                throw $e;
            }*/
        }
    }
}