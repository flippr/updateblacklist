<?php

namespace BlacklistUpdate;

/**
 * Created by PhpStorm.
 * User: josh
 * Date: 10/22/15
 * Time: 7:39 PM
 */

use BlacklistUpdate\Console\Command\RoleUpdateCommand;
use BlacklistUpdate\Console\Command\EmailUpdateCommand;
use Symfony\Component\Console\Application as BaseApplication;

class Application extends BaseApplication
{
    const NAME    = 'Blacklist Update Console Application';
    const VERSION = '1.0';

    public function __construct()
    {
        parent::__construct(static::NAME, static::VERSION);
        $roleUpdateCommand = new RoleUpdateCommand();
        $emailUpdateCommand = new EmailUpdateCommand();
        $this->add($roleUpdateCommand);
        $this->add($emailUpdateCommand);
    }
}